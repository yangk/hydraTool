package com.hydra.tool.object;

import com.hydra.tool.system.Sys;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

/**
 * Created by ZhengGong on 15/3/12.
 * Description 用于生成类中的属性对应的String常量 和 mapper中的where
 */
public class PojoConstantsUtil {
    private static final String PREFIX = "public static final String ";

    public static void console(Class<?> clazz) {
        Field[] fields = clazz.getDeclaredFields();
        StringBuilder fieldConstantsSb = new StringBuilder();

        for (Field field : fields) {
            if (!Modifier.isStatic(field.getModifiers())) {
                fieldConstantsSb.append(buildField(field));
            }
        }

        Sys.pl(fieldConstantsSb.toString());
    }

    private static String buildField(Field field) {
        return PREFIX + upperCaseTo_(field.getName()).toUpperCase() + " = " + '"' + field.getName() + "\";\n";
    }

    public static String upperCaseTo_(String word) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < word.length(); i++) {
            char c = word.charAt(i);
            if (!Character.isLowerCase(c) && i != 0) {
                sb.append("_");
            }
            sb.append(c);
        }
        return sb.toString();
    }
}
